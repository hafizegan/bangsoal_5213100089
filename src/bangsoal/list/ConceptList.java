/*
 * The MIT License
 *
 * Copyright 2015 hafizegan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package bangsoal.list;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author hafizegan
 */
/* Kali ini, saya melakukan refacotoring kelas ConceptListModel.java menjadi kelas ConceptList.java. 
   Kelas tersebut hanya berisikan interface dari method kelas yang direfactor.
   Hal tersebut dikarenakan apabila terdapat kelas lain yang ingin mengimplementasikan behaviour 
   dari kelas ConceptList.java , kelas baru tersebut dapat mengimplementasikan method yang sama dengan kelas ConceptListModel
   melalui kelas ConceptList.java yang berisikan interface, tanpa harus merubah kelas ConceptListModel.java*/
public interface ConceptList extends ListModel {

    void addListDataListener(ListDataListener l);

    Object getElementAt(int index);

    int getSize();

    void removeListDataListener(ListDataListener l);
    
}
